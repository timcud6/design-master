import './App.css';
import ScreenHome from "./ScreenHome/ScreenHome";
import Navbar from "../src/ScreenHome/AllBlocks/Navbar";
import { render } from "react-dom";
import {BrowserRouter, Routes, Route,} from "react-router-dom";
import ScreenFeatures from "./ScreenFeatures/ScreenFeatures";
import ScreenShowcase from "./ScreenShowcase/ScreenShowcase";
import ScreenPricing from "./ScreenPricing/ScreenPricing";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Navbar/>
        <Routes>
          <Route path="/" element={<ScreenHome/>} />
          <Route path="features" element={<ScreenFeatures />}/>
          <Route path="showcase" element={<ScreenShowcase />}/>
          <Route path="pricing" element={<ScreenPricing />}/>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
