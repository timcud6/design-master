import React from 'react';
import {BlockSecond, Icons, SecondPart} from "../Styles/Navbar.styled";
import {Paragraph, ParagraphRed, Title, TitleSmall} from "../Styles/Paragraph";
import {DataForSecondBlock} from "../data/DataForSecondBlock";

const SecondBlock = () => {
  return (
    <div>
      <BlockSecond>
        <ParagraphRed>
          WORK BETTER
        </ParagraphRed>
        <Title>
          For Your Business
        </Title>
        <Paragraph>
          We did research what your company needs and<br/>
          here we are providing all of them just for you
        </Paragraph>
      </BlockSecond>
      <SecondPart>
        {DataForSecondBlock.map((item) => {
          return (
            <div key={item.id}>
              <Icons>
                <img src={item.img}/>
                <div>
                  <TitleSmall>
                    {item.title}
                  </TitleSmall>
                  <Paragraph padding={15}>
                    {item.subtitle}
                  </Paragraph>
                  <Paragraph padding={1}>
                    {item.subtitle2}
                  </Paragraph>
                </div>
              </Icons>
            </div>
          )
        })}
      </SecondPart>
    </div>
  )
};

export default SecondBlock;