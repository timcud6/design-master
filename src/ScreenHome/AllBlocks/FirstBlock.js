import React from 'react';
import {ItemAlign} from "../Styles/Navbar.styled";
import {Paragraph, Title} from "../Styles/Paragraph";
import {ButtonBlue} from "../Styles/Button";
import {BigImage, ImageBrand, Smallimage, Smallimage2} from "../Styles/Images";
import image_firstblock from '../images/image_firstblock.png';
import image_first1 from '../images/image_first1.png';
import image_first2 from '../images/image_first2.png';
import brand from '../images/brand.png';

const FirstBlock = () => {
  return(
    <>
      <ItemAlign>
        <div>
          <Title size={55} sizeMax={38} align={'start'}>
            Manage Payroll
            Like an Expert
          </Title>
          <Paragraph align={'start'}>
            Payna is helping you to setting up the payroll without
            required any finance skills or knowledge before
          </Paragraph>
          <ButtonBlue right={220} marginLeft={220}>
            Get Started
          </ButtonBlue>
        </div>
        <Smallimage>
            <img src={image_first1}/>
        </Smallimage>
        <BigImage>
            <img src={image_firstblock}/>
        </BigImage>
        <Smallimage2>
            <img src={image_first2}/>
        </Smallimage2>
      </ItemAlign>
      <ImageBrand>
        <img src={brand}/>
      </ImageBrand>
    </>
  )
};

export default FirstBlock;