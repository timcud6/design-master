import React from 'react';
import {NavbarStyled, ItemAlign, ItemAlign1} from "../Styles/Navbar.styled";
import {Paragraph1, Paragraph2} from "../Styles/Paragraph";
import {ButtonGrey, Image, Logo} from "../Styles/Button";
import ellipse1 from '../images/ellipse1.png';
import ellipse2 from '../images/ellipse2.png';
import {dataNavbar} from "../data/dataNavbar";
import {Link} from "react-router-dom";

const Navbar = () => {
  return(
    <NavbarStyled>
      <div>
          <Paragraph1>
            <ItemAlign>
          <Logo>
            <Image margin={10}>
              <img src={ellipse1} />
            </Image>
            <img src={ellipse2}/>
          </Logo>
            Payna
            </ItemAlign>
          </Paragraph1>
      </div>
      <div>
        <ItemAlign1>
          {dataNavbar.map((item) => {
        return(
          <Link key={item.id} to={item.url} alt={item.title}>
            <Paragraph2>
              {item.title}
            </Paragraph2>
          </Link>
        )})}
        </ItemAlign1>
      </div>
      <div>
        <ButtonGrey>
          Sign in
        </ButtonGrey>
      </div>
    </NavbarStyled>
  )
};

export default Navbar;