import React from 'react';
import {Paragraph, ParagraphRed, Title} from "../Styles/Paragraph";
import {ButtonBlue, ButtonGrey} from "../Styles/Button";
import ReactPlayer from 'react-player';
import {AlignButton, BlockThird, Video} from "../Styles/Navbar.styled";

const ThirdBlock = () => {
    return (
      <div>
        <BlockThird>
          <Video>
            <ReactPlayer url='https://www.youtube.com/watch?v=lbtte7iTS9g'/>
          </Video>

          <div>
        <ParagraphRed top={50} text={'start'}>
          SAVE MORE TIME
        </ParagraphRed>
        <Title>
          And Boost Productivity
        </Title>
        <Paragraph>
          Your employees can bring any success into your
        </Paragraph>
        <Paragraph padding={1}>
          business, so we need to take care of them
        </Paragraph>

        <AlignButton>
          <ButtonGrey>
            Email Address
            <p> . . . . . . . . . . . . .</p>
          </ButtonGrey>
          <ButtonBlue top={1} left={-25} marginLeft={-75}>
          Get Started
        </ButtonBlue >
          </AlignButton>
          </div>
        </BlockThird>
      </div>
    )
  };
export default ThirdBlock;