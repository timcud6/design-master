import React from "react";
import data1 from '../images/data1.png';
import data2 from '../images/data2.png';
import data3 from '../images/data3.png';
import data4 from '../images/data4.png';
import data5 from '../images/data5.png';
import data6 from '../images/data6.png';

export const DataForSecondBlock =[
  {
    id:1,
    title:"Share Insights",
    subtitle: "Working together with your" ,
     subtitle2: "team to make decisions",
    img: data1,
  },
  {
    id:2,
    title:"Track Leads",
    subtitle: "See where your money goes ",
      subtitle2: "and comes in business",
    img: data2,
  },
  {
    id:3,
    title:"Offline Mode",
    subtitle: "Use the feature while off ",
      subtitle2: "from internet? sure can",
    img: data3,
  },
  {
    id:4,
    title:"Kanban Mode",
    subtitle: "Organize the report that",
    subtitle2: " easy to be understand",
    img: data4,
  },
  {
    id:5,
    title:"Reward System",
    subtitle: "Motivate your team working",
      subtitle2: "harder and receive a gift",
    img: data5,
  },
  {
    id:6,
    title:"189 Country",
    subtitle: "Working together worldwide",
      subtitle2: "people from anywhere",
    img: data6,
  },
]