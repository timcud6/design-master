import React from "react";

export const dataNavbar=[
  {
    id:1,
    title:"Home",
    url: "/",
  },
  {
    id:2,
    title:"Features",
    url: "/features",
  },
  {
    id:3,
    title:"Showcase",
    url: "/showcase",
  },
  {
    id:4,
    title:"Pricing",
    url: "/pricing",
  },
]