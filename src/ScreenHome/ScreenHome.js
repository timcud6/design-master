import React from 'react';
import FirstBlock from "./AllBlocks/FirstBlock";
import SecondBlock from "./AllBlocks/SecondBlock";
import ThirdBlock from "./AllBlocks/ThirdBlock";

const ScreenHome = () => {
  return(
    <div>
      <FirstBlock/>
      <SecondBlock/>
      <ThirdBlock/>
    </div>
  )
};

export default ScreenHome;