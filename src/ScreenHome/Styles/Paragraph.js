import styled from "styled-components";

export const Paragraph = styled.p`
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 32px;
  padding-top: ${(props) => props.padding ? props.padding : 20}px;
  text-align: start;
  color: #575455;

  @media screen and (max-width: 834px){
    text-align: center;
  }

  @media screen and (max-width: 425px){
    display: flex;
    text-align: ${(props) => props.align ? props.align : 'center'};

  }
`;

export const ParagraphRed = styled.p`
  padding-top: ${(props) => props.top ? props.top : 0}px;
  font-size: 16px;
  font-style: normal;
  font-weight: 700;
  line-height: 24px;
  text-align: ${(props) => props.text ? props.text : 'center'};
  color: #F75C4E;

  @media screen and (max-width: 834px){
    text-align: center;
    padding-top: 40px;
  }
`;

export const Paragraph1 = styled.p`
  font-size: 28px;
  font-weight: 600;
  line-height: 42px;
  text-align: left;
  color: #070F18;
`;

export const Paragraph2 = styled.p`
  font-size: 16px;
  font-weight: 400;
  line-height: 24px;
  text-align: center;
  padding-right: 30px;
  color: #070F18;
  
  &:hover{
    text-decoration: underline;
    cursor: pointer;
    font-weight: 600;
  }
`;

export const Title = styled.p`
  font-size: ${(props) => props.size ? props.size : 36}px;
  font-weight: 700;
  line-height: 83px;
  text-align: left;

  @media screen and (max-width: 834px){
    text-align: center;
  }

  @media screen and (max-width: 425px){
    text-align: ${(props) => props.align ? props.align : 'center'};
    font-size: ${(props) => props.sizeMax ? props.sizeMax : 28}px;
    line-height: 54px;
    
  }
`;

export const TitleSmall = styled.p`
  margin-top: 50px;
  font-size: 20px;
  font-weight: 600;
  line-height: 30px;
  text-align: left;
`



