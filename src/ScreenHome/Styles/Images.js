import styled from "styled-components";

export const Smallimage = styled.div`
  display: flex;
  margin-right: -110px;
  margin-top: 70px;
  z-index: 2;
  padding-left: 75px;
  @media screen and (max-width: 834px){
    display: none;
  }
  
  @media screen and (max-width: 425px){
    display: none !important;
  }
`;

export const Smallimage2 = styled.div`
  display: flex;
  margin-left: -170px;
  z-index: 2;
  padding-bottom: 150px;

  @media screen and (max-width: 834px){
    display: none;
  }

  @media screen and (max-width: 425px){
    display: none !important;
  }
`;

export const BigImage = styled.div`
  display: flex;
  flex-direction: column;
  padding-top: 70px;

  @media screen and (max-width: 834px){
    display: none;
  }

  @media screen and (max-width: 425px){
    display: none !important;
  }
`;

export const ImageBrand = styled.div`
  display: flex;
  flex-direction: column;
  padding: 80px 125px;
  @media screen and (max-width: 425px){
    display: none;
  }
  
`;

