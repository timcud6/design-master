import styled from "styled-components";

export const NavbarStyled = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 40px 120px;
  text-decoration: none;
  
  
  @media screen and (max-width: 834px){
    display: flex;
    padding: 45px 0px ;
  }
  
  `

export const ItemAlign = styled.div`
  display: flex;
  padding-left: 120px;
  flex-direction: row;
  align-items: center;
  @media screen and (max-width: 834px){
    padding-left: 60px;
    padding-right: 60px;
  }
  @media screen and (max-width: 425px){
    padding-right: 24px;
    padding-left: 24px;
   div{
     display: flex;
     flex-direction: column;
     justify-content: flex-start ;
     text-align: start;
     
   }
}
  
  `

export const ItemAlign1 = styled(ItemAlign)`
  @media screen and (max-width: 834px){
    display: none;
  }
`


export const BlockSecond = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;

  
  
  `

export const SecondPart = styled.div`
  padding-top: 40px;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: row;
  flex-wrap: wrap;
`

export const Icons = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  
  div{
    padding-left: 45px;
    padding-right: 70px;
  }

  @media screen and (max-width: 834px){
    div{
      padding-left: 25px;
      padding-right: 40px;
    }
  }
  
`

export const BlockThird = styled.div`
  padding-top: 100px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding-bottom: 100px;

  @media screen and (max-width: 834px){
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    text-align: center;
  }

  @media screen and (max-width: 425px) {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    text-align: center;
    padding-right: 24px;
    padding-left: 24px;
  }
  
`

export const Video = styled.div`
  padding-left: 50px;

  @media screen and (max-width: 834px) {
    padding-left: 10px;
    display: flex;
    width: 300px;
    height: 360px;
  }
  
`

export const AlignButton = styled.div`
  padding-top: 20px;
  display: flex;
  flex-direction: row;
  p{
    display: flex;
    flex-direction: row;
  }

  @media screen and (max-width: 425px) {
    display: flex;
    flex-direction: column;
   

  }
  
`



