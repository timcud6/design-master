import styled from "styled-components";

export const ButtonGrey = styled.p`
  padding: 12px 34px;
  background-color: #F5F6FB;
  border-radius: 50px;
  color: #575455;
  cursor: pointer;
  
  &:hover{
    background-color: lightgray;
    font-weight: 600;
    p {
      color: lightgray;}
  }
  
  p{
    color: #F5F6FB;
  }

  @media screen and (max-width: 834px){
    display: flex;
    justify-content: space-around;
    margin-right: 50px;
  }

  @media screen and (max-width: 425px){
    display: none;
    margin-right: 20px;
    margin-left: 20px;
    margin-bottom: 15px;
  }
`;

export const ButtonBlue = styled.p`
  margin-right: 200px;
  margin-top: ${(props) => props.top ? props.top : 40}px;
  margin-left: ${(props) => props.left ? props.left : 0}px;
  padding: 12px 34px;
  background-color: #1F7CFF;
  border-radius: 50px;
  color: white;

  &:hover{
    cursor: pointer;
    background-color: #1d5cb5;
    font-weight: 600;
  }

  @media screen and (max-width: 834px){
    display: flex;
    justify-content: space-around;
    margin-right: ${(props) => props.right ? props.right : 0}px;
    margin-left: ${(props) => props.marginLeft ? props.marginLeft : 0}px;
  }

  @media screen and (max-width: 425px){
    margin-right: 20px;
    margin-left: 20px;
  }
`;

export const Logo = styled.p`
  display: flex;
  margin-right: 20px;
  padding: 5px 10px;
  background-color: #F5F6FB;
  border-radius: 50px;
`;

export const Image = styled.p`
  display: flex;
  margin-right: 5px;
`;

export const RedImage = styled.img`
  
`;